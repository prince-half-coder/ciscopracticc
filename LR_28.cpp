#include <iostream>
using namespace std;

int main(){
  int vector[] = {1, 7, 3, 8, 3, 7, 1};
  bool ispalindrome = true;
  int n = sizeof(vector) / sizeof(vector[0]);

  for(int i = 0; i < n - 1; i++) {
  	if(vector[i] == vector[n-1-i]) {
      ispalindrome = 1;
    } else {
      ispalindrome = 0;
      break;
    }
  }

  ispalindrome ? cout << "It's a palindrome\n" : cout << "It isn't a palindrome\n";

  return 0;
}
