#include <iostream>
using namespace std;

bool isLeap(int year) {
	if (year % 4) {
	   return false;
	} else if (year % 100) {
	   return true;
	} else if (year % 400) {
	   return false;
	} else {
    return true;
  }
}

int main() {
  for(int year = 1995; year < 2017; year++){
    cout << year << " -> " << isLeap(year) << endl;
  }
  return 0;
}
