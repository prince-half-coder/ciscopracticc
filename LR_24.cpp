#include <iostream>
using namespace std;
int main(void) {
  unsigned short int val;
  bool ispalindrome = true;
  cout << "value = "; cin >> val;
  for (int i = 7; i >= 0; i--) {
	  if(((val & (1 << (15 - i))) == 0) != ((val & (1 << i)) == 0)) {
      ispalindrome=0;
      break;
    }
  }
  if(ispalindrome){
    cout << val << " is a bitwise palindrome\n";
  } else {
    cout << val << " is not a bitwise palindrome\n";
  }
  return 0;
}
