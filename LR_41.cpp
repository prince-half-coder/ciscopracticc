#include <iostream>
using namespace std;
int increment(int &x, int y = 1) {
	return x += y;
}
int main() {
  int var = 0;
  for(int i = 0; i < 10; i++) {
	  if (i % 2) increment(var);
	  else increment(var,i);
  }
  cout << var << endl;
  return 0;
}
