#include <iostream>
using namespace std;

int main () {
  int c0, i = 0;

  cout << " Input number => ";
  cin >> c0;

  while (c0 != 1) {
    c0 % 2 == 1 ? c0 = (3 * c0) + 1 : c0 /= 2;
    i++;
    cout << c0 << endl;
  }

  cout << "Steps => " << i << endl;

  return 0;
}
