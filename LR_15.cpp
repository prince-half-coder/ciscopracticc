#include <iostream>
using namespace std;

int main() {
  double pi4 = 0;
  long double n;

  cout << "Number of iterations => "; cin >> n;

  double znak = 1, k = 1;
  for (int i = 1; i <= n; i++){
    pi4 += (znak * (1 / k));
    znak = -znak;
    k += 2;
  }

  cout.precision(20);
  cout << "PI => " << pi4 * 4 << endl;

  return 0;
}
