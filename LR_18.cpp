#include <iostream>
using namespace std;

int main(){
  int step;

  cout << "Input step => "; cin >> step;
  if (step > 0 && step < 100){
    cout << "+";
    for (int i = 0; i < step; i++){
      cout << "-";
    }
    cout << "+\n";
    for (int i = 0; i < step; i++){
      cout << "|";
      for (int j = 0; j < step; j++){
        cout << " ";
      }
      cout << "|\n";
    }
    cout << "+";
    for (int i = 0; i < step; i++){
      cout << "-";
    }
    cout << "+\n";
  } else {
    cout << "Sorry, the side size is too big\n";
  }

  return 0;
}
