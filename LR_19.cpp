#include <iostream>
using namespace std;

int main(){
  int n;
  cout << "Input N => ";  cin >> n;
  if (n > 2 && n < 9){
    for (int i = 1; i <= n; i++){
      for (int l = 0; l < 3; l++){
        for (int j = 1; j <= n - i; j++){
          cout << " ";
        }
        for(int j = 1; j <= (2 * i) - 1; j++){
          if (i < n){
            if (j == 1 || j == (2 * i) - 1){
              cout << "*";
            } else {
              cout << " ";
            }
          } else {
            cout << "*";
          }
        }
        for (int j = 1; j <= (n - i) + 5; j++){
          cout << " ";
        }
      }
      cout << endl;
    }
  } else {
    cout << "Wrong input :(\n2 < N < 9!\n";
  }
  return 0;
}
