#include <iostream>
#include <time.h>
using namespace std;
int main(){
  struct clock{
    int hours;
    int minutes;
    int minutesInterval;
  }clock;
  cout << "Input time =>";
  cin >> clock.hours >> clock.minutes >> clock.minutesInterval;
  clock.minutes += clock.minutesInterval;

  if (clock.hours < 0 || clock.minutes < 0 || clock.minutesInterval < 0){
    cout << "Error input\n";
  } else {
    while (clock.minutes >= 60){
      clock.minutes -= 60;
      clock.hours += 1;
    }
    while (clock.hours >= 24){
      clock.hours -= 24;
    }
    cout << clock.hours << ":" << clock.minutes << endl;
  }

  return 0;
}
